#include <Arduino_HTS221.h>


void setup()
{
    Serial.begin(9600);
    while (!Serial);

  if (!HTS.begin()) {
    Serial.println("Failed to initialize humidity temperature sensor!");
    while (1);
  }
}

void loop()
{
    float temp = celcius(A0);
    float temp2 = HTS.readTemperature();
    Serial.print("Celcius:\t");
    Serial.print(temp);
    Serial.print("\t");
    Serial.println(temp2);
    delay(500);
}

/**
 * Calculate the temperature in Cº based on the code from
 * http://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/
 * 
 **/
float celcius(int pin)
{
    static const float R1 = 10000;
    static const float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
    static float logR2, R2;
    static int Vo = 0;
    Vo = analogRead(pin);

    R2 = R1 * (1023.0 / (float)Vo - 1.0);
    logR2 = log(R2);
    return (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2)) - 273.15;
}
